LESS files structure
============================

Original description: http://www.vzhurudolu.cz/blog/29-organizace-css-2014

main.less
=========
Core file

base folder
===========
Typography

/base/text.less
/base/fonts.less
/base/table.less
/base/reset.less

core folder
===========

/core/mixins.less
/core/helpers.less
/core/variables.less

components folder
=================

/components/article-items.less
/components/buttons.less
/components/nav.less
/components/icons.less

layout folder
============

/layout/homepage.less
/layout/page.less


