					      <div class="row">		
					      	<?php require_once('components/bxslider.php') ?>					      	
					      	<div class="col-md-8 column-content-right">
					      		<div class="content-right">
					      			<div class="content-nav narrow">
					      				<ul class="subnav">
					      					<li class="brand">
					      						<a class="active" href="/">Galerie</a>
					      					</li>
					      					<li>
					      						<a href="/">foto</a>
					      					</li>
					      					<li>
					      						<a href="/">video</a>
					      					</li>
					      					<li>
					      						<a href="/">archiv galerie</a>
					      					</li>
					      				</ul>
					      			</div>
					      			<div class="content-text narrow">
					      				<div class="box wysiwyg">
					      					<h3>
					      						<a href="?layout=gallerydetail">Koncert filmové hudby - Fantasy večer</a>
					      					</h3>
					      					<div class="clearfix"></div>
					      					<div class="gallery gallery-detail">
					      						<?php $width = 132; ?>
					      						<?php $height = 87; ?>					      						
					      						<?php for ($i = 1; $i <= 3; $i++): ?>													
					      						<a rel="lightbox[roadtrip]" href="data/images/koncert/<?php echo $i ?>.jpg" class="item">
					      							<img class="grayscale grayscale-fade" width="<?php echo $width ?>" height="<?php echo $height ?>" src="data/images/koncert/<?php echo $i ?>.jpg?width=<?php echo $width ?>&height=<?php echo $height ?>" />
					      						</a>
					      						<?php endfor; ?>					      						
					      					</div>
					      					<div class="clearfix"></div>
					      				</div>
					      				<div class="clearfix"></div>
					      				<div class="box wysiwyg">
					      					<h3>
					      						<a href="?layout=gallerydetail">Back to the Future 3 - Film Philharmonic</a>
					      					</h3>
					      					<div class="clearfix"></div>
					      					<div class="gallery gallery-detail">
					      						<p><iframe allowfullscreen="" frameborder="0" height="315" src="//www.youtube.com/embed/0lQoo9aHEfw?rel=0" width="100%"></iframe></p>				      						
					      					</div>
					      					<div class="clearfix"></div>
					      				</div>
					      				<div class="clearfix"></div>

					      			</div>
					      		</div>
					      	</div>
					      </div>