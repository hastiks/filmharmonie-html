					      <div class="row">
					      	<?php require_once('components/bxslider.php') ?>					      	
					      	<div class="col-md-8 column-content-right">
					      		<div class="content-right">
					      			<div class="content-nav narrow">
					      				<ul class="subnav">
					      					<li class="brand">
					      						<a href="/" class="active">Kontakt</a>
					      					</li>
					      				</ul>
					      			</div>
					      			<div class="content-text narrow">
					      				<div class="box wysiwyg">
					      					<h1>Kontakt</h1>
					      					        <p>
            <p dir="ltr"><span style="line-height: 1.6em;">Produkce, management, prodej l&iacute;stků</span><br />
<span style="line-height: 1.6em;"><strong>Aneta Jone&scaron;ov&aacute;</strong></span><br />
<a href="mailto:produkce@filmharmonie.cz" style="line-height: 1.6em;">produkce@filmharmonie.cz</a></p>

<p dir="ltr">Ředitel orchestru<br />
<span style="line-height: 1.6em;"><strong>Matěj Leh&aacute;r</strong></span><br />
<a href="mailto:reditel@filmharmonie.cz" style="line-height: 1.6em;">reditel@filmharmonie.cz</a></p>

<p dir="ltr">Archiv<br />
<span style="line-height: 1.6em;"><strong>Kl&aacute;ra Herdov&aacute;</strong></span><br />
<a href="mailto:archiv@filmharmonie.cz" style="line-height: 1.6em;">archiv@filmharmonie.cz</a></p>

<p>&nbsp;</p>

        </p>


<div>
<form class="form-horizontal" role="form" action="/kontakt/?calendar-month=11&amp;calendar-year=2014&amp;do=contactForm-contactForm-submit" method="post" id="frm-contactForm-contactForm">    <!-- Jednoduché vykreslení chyb -->
    <!-- /Jednoduché vykreslení chyb -->

    <div class="form-group">
        <label class="col-sm-2 control-label" for="frmcontactForm-first_name">Jméno:</label>
        <div class="col-sm-6">
          <input class="form-control" placeholder="Jméno" type="text" maxlength="32" name="first_name" id="frmcontactForm-first_name" data-nette-rules="{op:':maxLength',msg:&quot;Max 32 znak\u016f&quot;,arg:32}" value="">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="frmcontactForm-last_name">Příjmení:</label>
        <div class="col-sm-6">
          <input class="form-control" placeholder="Příjmení" type="text" maxlength="32" name="last_name" id="frmcontactForm-last_name" data-nette-rules="{op:':maxLength',msg:&quot;Max 32 znak\u016f&quot;,arg:32}" value="">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="frmcontactForm-email">Email: *</label>
        <div class="col-sm-6">
          <input class="form-control" placeholder="Email" type="email" maxlength="96" name="email" id="frmcontactForm-email" required data-nette-rules="{op:':filled',msg:&quot;Email je povinn\u00fd&quot;},{op:':maxLength',msg:&quot;Max 96 znak\u016f&quot;,arg:96},{op:':email',msg:&quot;Zadejte platn\u00fd form\u00e1t emailov\u00e9 adresy.&quot;}" value="">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="frmcontactForm-subject">Předmět: *</label>
        <div class="col-sm-6">
          <input class="form-control" placeholder="Předmět" type="text" maxlength="255" name="subject" id="frmcontactForm-subject" required data-nette-rules="{op:':filled',msg:&quot;P\u0159edm\u011bt je povinn\u00fd&quot;},{op:':maxLength',msg:&quot;Max 255 znak\u016f&quot;,arg:255}" value="">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="frmcontactForm-content">Dotaz: *</label>
        <div class="col-sm-10">
          <textarea class="form-control" cols="40" rows="10" name="content" id="frmcontactForm-content" required data-nette-rules="{op:':filled',msg:&quot;Vypl\u0148e text dotazu&quot;}"></textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-push-2 col-sm-10">
          <input class="btn btn-danger" type="submit" name="send" id="frmcontactForm-send" value="Odeslat">
        </div>
    </div>
<div><input type="hidden" name="ip_address" id="frmcontactForm-ip_address" value="89.176.10.122"></div>
</form>
</div>      					

					      					<div class="clearfix"></div>
					      				</div>
					      				<div class="clearfix"></div>					      				
					      			</div>
					      		</div>
					      	</div>
					      </div>