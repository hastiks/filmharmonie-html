					      <div class="row">										      	
					      	<div class="col-md-12">
					      		<div class="">
					      			<div class="content-nav narrow gallery-nav">
					      				<ul class="subnav">
					      					<li class="brand">
					      						<a href="?layout=gallery">Galerie</a>
					      					</li>
					      					<li>
					      						<a class="active">Koncert filmové hudby - Fantasy večer</a>
					      					</li>
					      				</ul>
					      			</div>
					      			<div class="content-text gallery-content narrow">
					      				<div class="box wysiwyg">
					      					<h3>
					      						<a href="/clanek/fantasy-vecer.html">Koncert filmové hudby - Fantasy večer</a>
					      					</h3>
					      					<div class="clearfix"></div>
					      					<div class="gallery gallery-detail">
					      						<?php $width = 132; ?>
					      						<?php $height = 87; ?>					      						
					      						<?php for ($i = 1; $i <= 21; $i++): ?>													
					      						<a rel="lightbox[roadtrip]" href="data/images/koncert/<?php echo $i ?>.jpg" class="item">
					      							<img class="grayscale grayscale-fade" width="<?php echo $width ?>" height="<?php echo $height ?>" src="data/images/koncert/<?php echo $i ?>.jpg?width=<?php echo $width ?>&height=<?php echo $height ?>" />
					      						</a>
					      						<?php endfor; ?>				      						
					      					</div>
					      					<div class="clearfix"></div>
					      				</div>
					      				<div class="clearfix"></div>					      									      			
					      			</div>
					      		</div>
					      	</div>
					      </div>