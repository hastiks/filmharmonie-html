					      <div class="row">
					      	<?php require_once('components/bxslider.php') ?>					      	
					      	<div class="col-md-8 column-content-right">
					      		<div class="content-right">
					      			<div class="content-nav narrow">
					      				<ul class="subnav">
					      					<li class="brand">
					      						<a class="active" href="/">Koncerty</a>
					      					</li>
					      					<li>
					      						<a href="/">archiv koncertů</a>
					      					</li>
					      				</ul>
					      			</div>
					      			<div class="content-text narrow">
					      				<div class="box wysiwyg">
					      					<h3>
					      						<a href="?layout=article">Koncert filmové hudby - Fantasy večer</a>
					      					</h3>
					      					<div class="clearfix"></div>
					      					<img src="data/images/poster/1.jpg" class="left" />
					      					<p>
					      						Filmová filharmonie pro Vás přichystala další koncert filmové hudby, tentokrát v žánru Fantasy. Pojďte se nechat unést nejznámějšími melodiemi fantasy filmů. Koncert proběhne v: 14.11.2014 v sále Pražsk…
					      						<a href="?layout=article">Číst více...</a>
					      					</p>
					      					<div class="clearfix"></div>
					      				</div>
					      				<div class="clearfix"></div>
					      				<div class="box wysiwyg">
					      					<h3>
					      						<a href="?layout=article">Vesmírná odysea, 21.6.2014 Praha</a>
					      					</h3>
					      					<p>
					      						21. června 2014 ve 21:21 se nádvoří Lichtenštejnského paláce, sídla HAMU, promění v jediné letní kino na Malé straně. Promítání kultovního filmu 2001: Vesmírná odysea doprovodí…
					      						<a href="?layout=article">Číst více...</a>
					      					</p>
					      				</div>
					      				<div class="clearfix"></div>
					      				<div class="box wysiwyg">
					      					<h3>
					      						<a href="?layout=article">Westernový koncert 20. 4. 2014 (Praha)</a>
					      					</h3>
					      					<p>
					      						Western, žánr amerického dobrodružného filmu, je osobitý svým hudebním ztvárněním. Přeneste se s Filmovou filharmonií prostřednictvím westernové hudby do Divokého západu mezi kovboje, indiány a zlatokopy a poslechněte si nejznámější melodie díky symfonickému orchestru.
					      						<a href="?layout=article">Číst více...</a>
					      					</p>
					      				</div>
					      				<div class="clearfix"></div>
					      			</div>
					      		</div>
					      	</div>
					      </div>