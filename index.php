<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/icon/favicon.ico">

    <title>Filmová filharmonie</title>

    <!-- Bootstrap core CSS -->
    <!--
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="bower_components/gray/css/gray.min.css" rel="stylesheet">
    <link href="bower_components/lightbox2/css/lightbox.css" rel="stylesheet">
    -->
    <link href="css/build.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css?v=4" rel="stylesheet">

    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300italic,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>


    <!-- Add conditional for IE7 + 8 support -->  
    <!--[if lte IE 8]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--    
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>        
    <script src="bower_components/bxslider-4/jquery.bxslider.min.js"></script>            
    <script src="bower_components/lightbox2/js/lightbox.min.js"></script>
    <script src="bower_components/respond/dest/respond.min.js"></script>
    -->    
    

    <!--[if (!IE)|(gt IE 8)]><!-->
      <script src="js/lib/jquery/2/jquery.min.js"></script>
    <!--<![endif]-->

    <!--[if lte IE 8]>
      <script src="js/lib/jquery/1/jquery.min.js"></script>
    <![endif]-->

    <script src="js/build.js"></script>
    <script src="js/main.js?v=4"></script>     

  </head>

  <body>

  	<div class="wrapper">

  		<div class="container">
  			<div class="row logo">
  				<div class="col-md-8 logo-box">
            <a href="/">
  					 <img src="images/logo.png" alt="Filmová filharmonie" title="Filmová filharmonie" />
            </a>
  				</div>
          <div class="col-md-3 language-box">
            <a href="" class="lang en">english</a>
          </div>
          <div class="col-md-1">
          </div>          
  			</div>
  			<div class="row">  							
  				<div class="col-md-8 column-left top-menu">
  					<?php require_once('components/menu.php') ?>

  					<!-- CONTENT start -->
				    <div class="content">

	  					<?php 
	  						if (isset($_GET['layout'])) {
	  							require_once('layouts/'.$_GET['layout'].'.php');
	  						} else {
	  							require_once('layouts/index.php');
	  						}
	  					?>

				    </div>
				    <!-- CONTENT end -->  					

				</div>				
  				
  				<?php require_once('components/calendar.php') ?>
  				<?php require_once('components/social.php') ?>
  				
	    	</div>

	    	<?php require_once('components/footer.php') ?>

		</div>


	</div>
    
  </body>
</html>