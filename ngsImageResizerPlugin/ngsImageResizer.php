<?php

ini_set('memory_limit', '512M');
// Paths
define('SF_THUMBNAIL_PLUGIN_PATH', dirname(__FILE__) . '/../plugins/sfThumbnailPlugin/lib');

// Included images
define('IMAGE_ERROR_CONFIG', base64_decode('iVBORw0KGgoAAAANSUhEUgAAAGQAAAA8CAYAAACQPx/OAAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB9gMExE1C53i8+sAAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAAJNklEQVR42u2bbUxTZxvHf31BSrEhTquQGg1qrMPK5rZEEzdfqJmgRgO2mpiYILLhEl++miUL05hsH1x8ySI2ViQaM5X2i0YtanzJYgxmEa2iiAXNFASKYidgR8/peT484YTagoc9MPHx/D/1XPfLdd33/1zXfd1376ORJElCxYiBVp0ClRAVA0Df96G4uFidkXcAt9udmJA3C1UMP950AjVkqWuICpUQlRAVQ5JlDYS6ujqam5sJh8OIovh2prVaMjMzycrKUmd5OAhpbm4mOzsbg8GAVjuwYwmCgCAI3Lhx450QUlxc/N5mi4oJCYfDGAwGqqur+euvv2hvb8dsNhOJRJAkCVEUaWhoID09ndTUVJYtW8a7OpV5n1N3xYREo1G0Wi0ajYZQKITZbKazs5Pu7m4EQUCv1zNx4kRaWlowmUyoR2TDTAiAJEloNBoEQaCnp4fOzk56enoQBAFJkjCZTIRCIXQ6HZIkvZWU4uJitmzZwqFDhxg3bhw//PADAHfu3MHr9dLS0oLZbMbhcPDJJ5/I7W7evInL5UIURZKTk5k6dSpr1qzBYrHEhaz+Th+ys7PZsmWLIn392TkiCNFqtUSjUZmUnp4eRFFEkiQ5fGm1WsUeUl1dzc8//0wwGASgsbGRw4cPU1RUhNVqpbGxkYMHDzJ69GimTp0KgMvloqSkhM8++4xwOMz169dxuVzs2LFDUfiqqqriiy++UKwvkZ0jJu3VaDQkJSUBoNPpEEURQRDQ6XQApKWlyb+VIDc3l5SUFCZNmgTA2bNncTgc2Gw2kpKSsFqtrF69mnPnzsltRFEkJSUFAIPBwKJFixKSkQi1tbUkJyczduxYxfoS2TkiCOl9+8eNG0d7ezuCIJCSkoLRaESj0fDs2TMsFstbs7C+SE9Pj3kOBAIx4QJg5syZNDQ0yM8LFy7E5/Nx8eJF6uvriUQiinS9ePGCq1evsmDBgkHpS2TnOw9ZvSFIo9EwZswYJEni8uXL/P3334iiyKhRo5g7dy5jx45VtE/pRa+39aK7u5utW7cm9MxeOJ1OysvLOXfuHKFQiOTkZL7//nt5DUkEURQpLy9n3bp1MX0p0ZfIzhGzhvh8PqLRKNFolNevX2Oz2ZAkiaamJqqrq3n16hV6vR6Px4PBYCAajbJ8+XLFOtLS0ti+fTtGo7HfOsnJyXz33XcAtLW1cefOHX799Vd++umnftt4PB5sNhsTJkwYtL4RmWX1Zlj5+fmyrLOzk1AoxPPnzykoKMDv9xMMBvnyyy/JyMigrq6OGzduYDAYWLx4sSI906dPx+/3M3fuXFn29OlT3G43P/74Y1z98ePHk5OTw2+//dZvnzU1NbS0tOB0Ov9nfSPmLEuj0cRlTqNHj8ZisZCdnY1er+fWrVvMmzePp0+f4vV65Y2iz+dTbNDXX39NZWUlfr+fSCTCkydPqKioIDc3V65TWlrK5cuXCYfD8oT3l0gEg0ECgQCbNm1KuLYp0TciPaQ3/Fit1rh1pRddXV2YzWbmz58vy3bu3Mnz588VGzR58mQKCwvxer08e/aMtLQ0cnNzY97gb7/9lmPHjnHy5EkikQg6nY6SkpKE/R05coT79+9TVVWVMCVWou/fhKbvrZOBzoDu37/Py5cv6ejo6Lez06dPY7fbefz4MaFQiEgkQiQSQRRF9uzZo27DFZy7KfaQjz/+WFFY+/333zEajUQiEcLhMK9evWLlypXqzA/XxnAg5OXlYbfb6ejooLW1FY1Gw8qVK1mxYoU608OR9iqB3W7HbrerMzsSPESFSohKiAqVkA97UVevk6oeokIlRCVEhUqISogKlRCVkH8Fs2fP5sCBA+zfv5+0tLQP6iMi/Ug0qqSkBJfLhd/vRxTFD2pvNCIJ0ev11NTUqDt1pcjJySEvLw+TyUQgEODo0aO0trbK5Z9++in5+flMmDCB1tZWvF4vfr9fLne73ZSVlVFQUIDZbCYajeJyuaipqZHDU9+roG63O85LsrKycDqdZGRk0NHRwZkzZygsLBzQm2w2G6tWrSI9PZ1gMIjH44mza+/evWzYsIH29nZ27tyZUKZ0jInaDTkhn3/+OTk5OezevZtgMMhXX33Fxo0b2b59uzzodevWcfDgQQKBANOnT2fDhg0cOHCAhw8fyv3Y7Xb27t1LW1sbs2fPpqSkhI0bN/ZLQF9MmjSJoqIi3G43gUCArKyst4a1zMxM1q9fT3l5OQ8ePGDKlCl88803lJWV0djYKNebM2cO27Ztw2w29ytTOsZEfQ35oj5//nxOnDhBc3MzkUiES5cuyWQALF++nMrKSurq6hAEgXv37uH1elm6dGlMP8ePH6etrQ34760RvV75u7FkyRI8Ho+sw+/3U1lZOWCbpUuX4vF4qK2tRRAE6uvrOXnyJHl5eTH1fD4f4XCYJ0+e9CtTOsZEfQ05IZMnTyYQCAxYfvfu3RiZ3+8nMzMzRtbU1PSP46zVao3Tcfv27QHbTJs2La5ObW1tzIVqgJaWlri2b8qUjjFRX0MesoxGo3wfKhF0Oh1dXV0xsq6urribgYO5bvomTCZTnI7Ozs632r1v3744eTQajXkWBCGuzpsypWNM1NeQE9L7JdXr168Tlnd3d5OamhozQSaTqd/6/wRdXV1xOlJTUwdsEwqFKC0tHRI7hnOMgw5Zf/75J1OmTOm3/NGjR9hstrjs5vHjx0NGSF1dXdy3izNnzhywTX19PdnZ2TEyi8VCaWnpoPUP5xgHTciVK1dwOByYzWaSkpJYsGBBzKCqqqpwOp3MmDEDnU7HjBkzcDgcnD17dsgIOXPmDAUFBVitVlmH0+kc8COh8+fP43Q6mTVrlvz5XWFh4aCuuf4bYxx0yPrjjz8YM2YM27Ztw2g08vDhQ8rKymLe3mPHjrF27VrGjx9PW1sbR44c4cGDB0NGSFNTExUVFaxevRqLxcLLly85depUwsvUfT27oqKCVatWkZGRQSgUwufzUV1d/Y88dLjGGHeV9H3FRx99xObNm2NS8PcFfc/q3svT3l9++YWFCxfKX25lZGRQVFTEpUuX1LOsd4H9+/eTn5+Pw+FAp9PR2trKhQsXuHbtmkrIu0BDQwO7du36vzxcVP+gUglRoRKiEqJiSPYhKlQPUaESMrLxHztEld4AianKAAAAAElFTkSuQmCC'));
define('IMAGE_ERROR_404', base64_decode('iVBORw0KGgoAAAANSUhEUgAAAGQAAAA8CAYAAACQPx/OAAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB9gMExE5KeQ3/QMAAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAAIkklEQVR42u2bbWyL3xvHP21n654sMt1DJtiIeagRkXiYMCayeQw2EiLBxkhk3opEhkjwDpHYshmRIGGNIOhEiBDMCw9l1NQsnlZ7sNW6rdr77v1/1Uat2+79f0Z////5vtp9nXN/r7Pz7XWd65yeahRFURAIGWjFFAhBBPpA2M8PhYWFYkb+AioqKoIL8mujwODj1yAQKUusIQJCECGIwG+psvqC1Wrly5cvuFwuZFnuX2mtltTUVCZOnChmeTAE+fLlCxkZGej1erTavgNLkiQkSeLx48d/RZDCwsJ/bbWoWhCXy4Ver6empobv37/T0tKCwWDA4/GgKAqyLPPu3TuSkpKIjo5myZIl/K1TmX9z6a5aEK/Xi1arRaPR4HA4MBgMOJ1Ourq6kCSJsLAwRowYgd1uJzY2FnFENsiCACiKgkajQZIk3G43TqcTt9uNJEkoikJsbCwOhwOdToeiKP2KUlhYSHFxMSdPnmT48OHs2bMHgBcvXmAymbDb7RgMBvLy8pgyZYr/vSdPnlBWVoYsy0RERDBmzBjWrl1LSkpKj5TV2+lDRkYGxcXFqvz1Ns6QEESr1eL1ev2iuN1uZFlGURR/+tJqtaojpKamhkOHDtHc3AxAfX09p06dYvPmzaSnp1NfX095eTkxMTGMGTMGgLKyMoqKipg2bRoul4uHDx9SVlbG/v37VaWv6upqpk+frtpfsHGGTNmr0WgYMmQIADqdDlmWkSQJnU4HQFxcnP9vNcjJySEyMpKRI0cCcP36dfLy8jAajQwZMoT09HTWrFnDjRs3/O/IskxkZCQAer2e+fPnBxUjGGpra4mIiCA+Pl61v2DjDAlBfJ/+4cOH09LSgiRJREZGEhUVhUajobGxkZSUlH6rsJ+RlJQU8Gyz2QLSBcCkSZN49+6d/zkrKwuz2cytW7eoq6vD4/Go8vXt2zfu3r3LvHnzBuQv2Dj/esrypSCNRsOwYcNQFIU7d+7w48cPZFkmPDycmTNnEh8fr2qf4oMv2nzo6upi586dQSPTh/z8fCorK7lx4wYOh4OIiAh2797tX0OCQZZlKisr2bBhQwCXGn/Bxhkya4jZbMbr9eL1eunu7sZoNKIoCp8/f6ampoaOjg7CwsKoqqpCr9fj9XpZunSpah9xcXHs27ePqKioXvtERESwfft2AJqamnjx4gXHjx/n4MGDvb5TVVWF0WgkMTFxwP5CssryVVgrV67025xOJw6Hg9bWVlatWoXFYqG5uZk5c+aQnJyM1Wrl8ePH6PV6Fi5cqMrPuHHjsFgszJw502/79OkTFRUV7N27t0f/hIQEFixYwPnz53vlfPr0KXa7nfz8/H/sL2TOsjQaTY/KKSYmhpSUFDIyMggLC+PZs2dkZmby6dMnTCaTf6NoNptVD2jRokVcvHgRi8WCx+Ph48ePnD59mpycHH+fkpIS7ty5g8vl8k94b4VEc3MzNpuNHTt2BF3b1PgLyQjxpZ/09PQe64oPnZ2dGAwG5s6d67cdOHCA1tZW1QMaNWoUGzduxGQy0djYSFxcHDk5OQGf4K1bt3L27FkuXLiAx+NBp9NRVFQUlO/MmTO8fv2a6urqoCWxGn9/Epqfb530dQb0+vVr2tvbaWtr65Xs6tWrZGdn09DQgMPhwOPx4PF4kGWZI0eOiG24inM31REyYcIEVWnt3r17REVF4fF4cLlcdHR0sGLFCjHzg7Ux7Au5ublkZ2fT1tbG169f0Wg0rFixguXLl4uZHoyyVw2ys7PJzs4WMxsKESIgBBGCCAhB/r8XdXGdVESIgBBECCIgBBGCCAhBhCD/esyePTvoVwyzZs3i8OHDlJaWsn//frKysv4rnt++D/lfRnJycsCNEx/i4+PJzc2lvLychoYGRo8ezbZt27Db7VitVtU8IkIGgPDwcDZt2sSpU6d6tGVmZnLlyhVsNhuSJGGz2bh8+TJTp04dEE9IRojRaGT16tUkJSXR3NxMVVUVFovF315RUcHRo0cpKCigpaWFAwcOBLUBTJ06lZUrV5KYmMjXr18xmUz9cvWG9evXc/v2bex2e4+2CRMm8OjRowBbXV1d0CjoiyfkBElNTWXTpk1UVlby5s0b0tLS2LJlCydOnKC+vt7fb8aMGezatQuDwdCrzWg0smHDBsrLy7HZbIwbN46CggJKS0t5+/Ztn1zB8r3X6+0x6T4kJSXR3t4eYGtvbychIWFAPCGXshYvXkxVVRW1tbVIkkRdXR0XLlwgNzc3oJ/ZbMblcvHx48debUuXLuXixYtYrVYkSeLVq1eYTCYWL17cL1ewfH/u3Llexx0dHd3j9qPb7Uav1w+IJ+QEGTt2LM+fPw+w1dbWBlxaBoKG+6+2UaNG8fLlywCbxWIhNTW1X65g+b6v66Zerzfo5XCfTS1PyKWsqKgojh07FvQf/hmSJPXo86tNp9PR2dkZYOvs7OxxwzAY10DzfWdnJ+Hh4bjd7gAxu7q6/si6MWiCOBwOSkpK6O7u/sdcXV1dREdH43Q6/bbY2NgBcWdmZpKZmRn0K4WKigq//fPnzwwdOpSWlhZ/+9ChQ2lsbBwQT8gJUldXR0ZGBjU1NX5bSkoKhYWF7Nu3b0Bc79+/x2g0BiygRqORhoYG1Ry9TdSvk/jy5UtSU1MDBElLS/PvQdTyhNwacvPmTfLz85k8ebL/J24bN24c0FVSH6qrq8nPz2f8+PHodDrGjx9PXl4e169f/+1p4sGDB+Tk5JCWloZOp2Ps2LEsW7aM+/fv//H90m+NkA8fPnD69GlWr15NcnIyDocDs9kcEDFqYbVaOXv2LOvWrSMhIYGmpibOnDnDmzdvfvskOJ1Orl27RlFREcOGDaOtrY1Lly7x7du3Py5Ij6ukAn8eP5+JidPeEIMQRAgiIAQRgggIQYQgAoO+DxEQESIgBAlt/Ada5DJWXfxBMwAAAABJRU5ErkJggg=='));

$contentTypes = array(
    'gif' => 'image/gif',
    'png' => 'image/png',
    'jpg' => 'image/jpeg',
);

$imgLoaders = array(
    'image/jpeg' => 'imagecreatefromjpeg',
    'image/pjpeg' => 'imagecreatefromjpeg',
    'image/png' => 'imagecreatefrompng',
    'image/gif' => 'imagecreatefromgif',
);

$imgCreators = array(
    'image/jpeg' => 'imagejpeg',
    'image/pjpeg' => 'imagejpeg',
    'image/png' => 'imagepng',
    'image/gif' => 'imagegif',
);

$png = false;

// Append debug parameter to disable custom error handler
if (isset($_GET['debug'])) {

    error_reporting(E_ALL);
} else {

    // Custom error handler
    function customError($errno, $errstr) {
        if ($errno & E_WARNING) {
            header('HTTP/1.1 500 Internal Server Error');
            header('Content-Type: ' . $contentTypes['png']);

            die(IMAGE_ERROR_CONFIG);
        }
    }
    //radsi nezobrazovat nic, nez tuto chybu
    error_reporting(0);
    //set_error_handler('customError');    
}



$cacheDir = dirname(__FILE__) . '/cache';
if (!file_exists($cacheDir)) {
    mkdir($cacheDir, 0777, true);        
}
@chmod($cacheDir, 0777);

$path = $imagePath = isset($_REQUEST['image']) ? $_REQUEST['image'] : null;

// File not found, send 404 header and image

if (!file_exists($imagePath)) {    
    header('HTTP/1.1 404 Not Found');
    header('Content-Type: ' . $contentTypes['png']);
    if (strstr($_SERVER['REQUEST_URI'], '_dev')) {
        die(IMAGE_ERROR_404);
    }
    exit();
}

// Must use strpos, IE add '; length = XXX' after If-Modified-Since header 
$originalNotModified = strpos(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : 0, gmdate("D, d M Y H:i:s", filemtime($imagePath)) . ' GMT') !== false;

$reloadImageCache = isset($_REQUEST['random']) ? $_REQUEST['random'] : false;


if (isset($_REQUEST['width']) || isset($_REQUEST['height'])) {

    $scale = true;
    $width = (integer) (isset($_REQUEST['width'])) ? $_REQUEST['width'] : false;
    $height = (integer) (isset($_REQUEST['height'])) ? $_REQUEST['height'] : false;
    $minheight = (integer) (isset($_REQUEST['minheight'])) ? $_REQUEST['minheight'] : false;
    $fit = isset($_REQUEST['fit']) ? true : false;
    $inflate = isset($_REQUEST['inflate']) ? true : false;
    $noEnlarge = isset($_REQUEST["noenlarge"]) ? $_REQUEST["noenlarge"] : false;
    $quality = 100;

    if ($inflate) {
        $scale = false;
    }

    // Construct image name
    $hash = $imageName = pathinfo($imagePath, PATHINFO_FILENAME);

    if (isset($_REQUEST['width'])) {
        $hash .= '_w' . $width;
    }
    if (isset($_REQUEST['height'])) {
        $hash .= '_h' . $height;
    }
    if (isset($_REQUEST['fit'])) {
        $hash .= '_h' . $fit;
    }
    if (isset($_REQUEST['inflate'])) {
        $hash .= '_inflate';
    }
    if ($noEnlarge) {
        $hash .= "_noenlarge";
    }

    $extension = pathinfo($imagePath, PATHINFO_EXTENSION);

    if (in_array($extension, array("png", "PNG"))) {
        $finalMime = "image/png";
        $png = true;
    } else if (in_array($extension, array("gif", "GIF"))) {
        $finalMime = "image/gif";
    } else {
        $finalMime = "image/jpeg";
        $extension = "jpg";
    }

    $cachePath = $cacheDir . DIRECTORY_SEPARATOR . $hash . '.' . $extension;

    $size = getimagesize($imagePath);
    $mime = $size["mime"];
    $oWidth = $size[0];
    $oHeight = $size[1];

    //zatim jsem to zakazal protoze browsery to pravdepodobne odesilaji pokazde jinak, radsi teda nacitat primo ze serverove cache
    $originalNotModified = true;

    if (!file_exists($cachePath) || !$originalNotModified || $reloadImageCache) {

        if (file_exists(SF_THUMBNAIL_PLUGIN_PATH . '/sfGDAdapter.class.php')) {
            include SF_THUMBNAIL_PLUGIN_PATH . '/sfGDAdapter.class.php';
        }
        if (!class_exists("sfThumbnail")) {
            require SF_THUMBNAIL_PLUGIN_PATH . '/sfThumbnail.class.php';
        }

        if ($fit && ($width || $height) && ($oWidth != $width)) {

            if (!$height) {
                $height = round(($oHeight / $oWidth) * $width);
            }
            if (!$width) {
                $width = round($height * ($oWidth / $oHeight));
            }

            if ($minheight) {
                if ($height < $minheight) {
                    $height = $minheight;
                }
            }
            
            //die(var_dump($width, $height));

            try {         
                if (!class_exists('Imagick')) throw new Exception("Imagick is not installed");
                $im = new Imagick($imagePath);                
                $im->cropThumbnailImage($width, $height);                
                $im->writeImage($cachePath);
            } catch (Exception $e) {                   
                $th = new sfThumbnail($width, $height, $scale, true, $quality);
                $th->loadFile($imagePath);
                $th->save($cachePath, $finalMime);
                $th->freeAll();
            }
        } else if ($oWidth != $width) {
            if ($noEnlarge) {

                $size = getimagesize($imagePath);
                if ($width > $size[0]
                        || $height > $size[1]) {
                    //do nothing

                    $cachePath = $imagePath;
                } else {
                    $th = new sfThumbnail($width, $height, $scale, true, $quality);
                    $th->loadFile($imagePath);
                    $th->save($cachePath, $finalMime);
                    $th->freeAll();
                }
            } else {

                $th = new sfThumbnail($width, $height, $scale, true, $quality);

                $th->loadFile($imagePath);

                $th->save($cachePath, $finalMime);

                $th->freeAll();
            }
        } else {            
            $cachePath = $imagePath;            
        }

        if ($cachePath != $imagePath) {
            //save info about cachePath to infoFile
            $infoFilePath = $cacheDir . DIRECTORY_SEPARATOR . $imageName . ".txt";
            $infoFile = fopen($infoFilePath, "a+");
            fwrite($infoFile, $cachePath . '###');
            fclose($infoFile);
        }
        
    }

    $path = $cachePath;
    
}

$info = pathinfo($path);

$etag = 'image-' . sha1($imagePath);

$notModified = $originalNotModified && strpos((isset($_SERVER['HTTP_IF_NONE_MATCH']) ? $_SERVER['HTTP_IF_NONE_MATCH'] : null), $etag);


if ($notModified) {  

    // Not modified send only header
    
        header('HTTP/1.1 304 Not Modified');
        header('Cache-control: private');
        header('Pragma: private');

        header('ETag: "' . $etag . '"');
        header('Expires: ' . date("D, d M y H:i:s O", strtotime('+1 day')));
        header('Content-Type: ' . $contentTypes[$info['extension']]);
    
    die();
} else {         

    // Send content with last modified date    
        header('HTTP/1.1 200 OK');    
        header('Cache-control: private');
        header('Pragma: private');

        header('ETag: "' . $etag . '"');
        header('Expires: ' . date("D, d M y H:i:s O", strtotime('+1 day')));            
        header('Content-Type: ' . $contentTypes[$info['extension']]);    
        header('Last-Modified: ' . gmdate("D, d M Y H:i:s", filemtime($imagePath)) . ' GMT');
        
        header('Content-Length: ' . filesize($path));
    

    // Flush all buffers before reading to avoid buffering file contents
    ob_end_flush();
    flush();

    readfile($path);

    die();
}


