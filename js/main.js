function bindBxSlider() {
	if ($('.bxslider li').length > 1) {
		$('.bxslider').bxSlider({
			controls: false,
			auto: true,
			mode: "fade"
		});
	}
}


function changeContentRightSize() {
	var 
	$contentRight = $('.content .content-right .content-nav'),
	$menu = $('.nav.navbar-nav'),
	$bxSlider = $('.bxslider'),
	$galleryNav = $('.gallery-nav'),
	$galleryHead = $('.gallery-content h3'),
	menuWidth = ($menu.width() - 5),
	$footer = $('footer'),
	$footerContent = $footer.find('.footer-content')
	;

	//if ($bxSlider.length > 0) {
		//minus margin a padding menu		
		$contentRight.width(menuWidth);
	//}

	if ($galleryNav.length > 0) {
		//zarovnani s menu
		$galleryNav.width(menuWidth + 10);
		//minus padding nadrazeneho boxu
		$galleryHead.width(menuWidth - 18);
	}
	if ($footer.height() < $footerContent.height()) {
		$footer.height($footerContent.height());
		$('body').css('margin-bottom', ($footerContent.height()+20)+'px');
	}	
}



$(document).ready(function(){	
	bindBxSlider();
	changeContentRightSize();


	$(window).resize(function () { 
		changeContentRightSize();		
	});

});