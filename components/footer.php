			<!-- footer start -->
			<footer class="footer">
				<div class="row">
					<div class="column-left footer-content">
						<div class="row">	    		
							<div class="col-md-8">
								<span class="partners-headering">Naši partneři</span>	
								<div class="partners">						
								<a target="_blank" title="Total film" href="http://totalfilm.cz/">						
										<img alt="logo totalfilm" src="images/partners/totalfilm.png?width=120&height=31">								
								</a>		
							
								<a target="_blank" title="Kudy z nudy" href="http://www.kudyznudy.cz/">														
										<img alt="logo kudyznudy" src="images/partners/kudyznudy.png?width=120&height=31">								
								</a>	
													
								<a target="_blank" title="Ticket pro" href="http://www.ticketpro.cz/">													
										<img alt="logo ticketpro" src="images/partners/ticketpro.png?width=120&height=31">									
								</a>	
													
								<a target="_blank" title="Informuji" href="http://www.informuji.cz/">													
										<img alt="logo informuji" src="images/partners/informuji.png?width=120&height=31">								
								</a>			
														
								<a target="_blank" title="Kultura hradec" href="http://www.kultura-hradec.cz/">													
										<img alt="logo hradec" src="images/partners/hradec.png?width=120&height=31">									
								</a>				

								<a target="_blank" title="Placky levně" href="http://www.plackylevne.cz/">							
										<img alt="placky levně" src="images/partners/plackylevne.png?width=120&height=31">							
								</a>				

								<a target="_blank" title="E-noty" href="http://www.enoty.eu/">							
										<img alt="E-noty" src="images/partners/enoty.jpg?width=120&height=31">							
								</a>														

								</div>																									
							</div>
							<div class="col-md-4">
								<div class="copy">
									Filmová filharmonie o. s. © <a title="http://websfera.cz/" href="http://websfera.cz/">Websfera</a> 2014
								</div>
							</div>
							<div class="clear"></div>
						</div>
					</div>					
				</div>
			</footer>
			<!-- footer end -->