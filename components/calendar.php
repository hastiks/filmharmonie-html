				<!-- calendar widget start -->
				<div class="col-md-3 column-right">
					<div class="calendar nav-row">
						<div class="cal-nav nav-row">
							<a class="arrow" href="/?calendar-currentMonth=11&do=calendar-previous">
								&lt;
							</a>
							<span>Listopad 2014</span>
							<a class="arrow" href="/?calendar-currentMonth=11&do=calendar-next">
								&gt;
							</a>
						</div>	

				        <div class="cal-box">
							<table cellpadding="0" cellspacing="0" class="calendar">				        	
				                <thead>
				                <tr class="calendar-row">
				                        <td class="calendar-day-head">Po</td>
				                        <td class="calendar-day-head">Út</td>
				                        <td class="calendar-day-head">St</td>
				                        <td class="calendar-day-head">Čt</td>
				                        <td class="calendar-day-head">Pá</td>
				                        <td class="calendar-day-head">So</td>
				                        <td class="calendar-day-head">Ne</td>
				                </tr>
				                </thead>
				                <tbody>
				                <tr class="calendar-row">
				                    <td class="day calendar-day-np">
				                            <div class="day-number">27</div>
				                    </td>
				                    <td class="day calendar-day-np">
				                            <div class="day-number">28</div>
				                    </td>
				                    <td class="day calendar-day-np">
				                            <div class="day-number">29</div>
				                    </td>
				                    <td class="day calendar-day-np">
				                            <div class="day-number">30</div>
				                    </td>
				                    <td class="day calendar-day-np">
				                            <div class="day-number">31</div>
				                    </td>
				                    <td class="day calendar-day weekend">
				                            <div class="day-number">1</div>
				                    </td>
				                    <td class="day calendar-day weekend">
				                            <div class="day-number">2</div>
				                    </td>
				                </tr>
				                <tr class="calendar-row">
				                    <td class="day calendar-day">
				                            <div class="day-number">3</div>
				                    </td>
				                    <td class="day calendar-day">
				                            <div class="day-number">4</div>
				                    </td>
				                    <td class="day calendar-day">
				                            <div class="day-number">5</div>
				                    </td>
				                    <td class="day calendar-day">
				                            <div class="day-number">6</div>
				                    </td>
				                    <td class="day calendar-day">
				                            <div class="day-number">7</div>
				                    </td>
				                    <td class="day calendar-day weekend">
				                            <div class="day-number">8</div>
				                    </td>
				                    <td class="day calendar-day weekend">
				                            <div class="day-number">9</div>
				                    </td>
				                </tr>
				                <tr class="calendar-row">
				                    <td class="day calendar-day">
				                            <div class="day-number">10</div>
				                    </td>
				                    <td class="day calendar-day">
				                            <div class="day-number">11</div>
				                    </td>
				                    <td class="day calendar-day">
				                            <div class="day-number">12</div>
				                    </td>
				                    <td class="day calendar-day">
				                            <div class="day-number">13</div>
				                    </td>
				                    <td class="day calendar-day event">
				                            <div class="day-number">
				                                <a href="/events/14/11/2014/">14</a>
				                            </div>
				                    </td>
				                    <td class="day calendar-day weekend event">
				                            <div class="day-number">
				                                <a href="/events/15/11/2014/">15</a>
				                            </div>
				                    </td>
				                    <td class="day calendar-day weekend">
				                            <div class="day-number">16</div>
				                    </td>
				                </tr>
				                <tr class="calendar-row">
				                    <td class="day calendar-day">
				                            <div class="day-number">17</div>
				                    </td>
				                    <td class="day calendar-day">
				                            <div class="day-number">18</div>
				                    </td>
				                    <td class="day calendar-day">
				                            <div class="day-number">19</div>
				                    </td>
				                    <td class="day calendar-day">
				                            <div class="day-number">20</div>
				                    </td>
				                    <td class="day calendar-day">
				                            <div class="day-number">21</div>
				                    </td>
				                    <td class="day calendar-day weekend">
				                            <div class="day-number">22</div>
				                    </td>
				                    <td class="day calendar-day weekend">
				                            <div class="day-number">23</div>
				                    </td>
				                </tr>
				                <tr class="calendar-row">
				                    <td class="day calendar-day">
				                            <div class="day-number">24</div>
				                    </td>
				                    <td class="day calendar-day">
				                            <div class="day-number">25</div>
				                    </td>
				                    <td class="day calendar-day">
				                            <div class="day-number">26</div>
				                    </td>
				                    <td class="day calendar-day">
				                            <div class="day-number">27</div>
				                    </td>
				                    <td class="day calendar-day">
				                            <div class="day-number">28</div>
				                    </td>
				                    <td class="today day calendar-day today weekend">
				                            <div class="day-number">29</div>
				                    </td>
				                    <td class="day calendar-day weekend">
				                            <div class="day-number">30</div>
				                    </td>
				                </tr>
				                <tr class="calendar-row">
				                </tr>
				                </tbody>
				            </table>

				        </div>

					</div>	
					<!--
					<div class="fb-feed">
						<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Ffilmovafilharmonie&amp;width&amp;height=427&amp;colorscheme=light&amp;show_faces=false&amp;header=true&amp;stream=true&amp;show_border=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:427px;" allowTransparency="true"></iframe>					
					</div>
					-->
				</div>
				<!-- calendar widget end -->