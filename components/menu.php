<!-- HEAD start -->
				    <nav class="navbar navbar-inverse nav-row" role="navigation">
				      <div class="">
				        <div class="navbar-header">
				          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				            <span class="sr-only">Toggle navigation</span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				          </button>
				          <a class="navbar-brand" href="/">Filmová filharmonie</a>
				        </div>
				        <div id="navbar" class="collapse navbar-collapse">
				          <ul class="nav navbar-nav">
				            <li><a class="active" href="/">novinky</a></li>
				            <li><a href="?layout=onas">o nás</a></li>
				            <li><a href="?layout=koncerty">koncerty</a></li>
				            <li><a href="?layout=gallery">galerie</a></li>
				            <li><a href="?layout=promedia">pro média</a></li>
				            <li><a href="?layout=contact">kontakt</a></li>
				          </ul>
				        </div><!--/.nav-collapse -->
				      </div>
				    </nav>
<!-- HEAD end -->