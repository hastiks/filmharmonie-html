module.exports = function(grunt) {

	var jsFiles = [      	
      	'bower_components/bootstrap/dist/js/bootstrap.min.js', 
      	'bower_components/bxslider-4/jquery.bxslider.min.js',
      	'bower_components/lightbox2/js/lightbox.min.js',
      	'bower_components/respond/dest/respond.min.js'	
	];

	var cssFiles = [
		'bower_components/bootstrap/dist/css/bootstrap.min.css',
		'bower_components/gray/css/gray.min.css',
		'bower_components/lightbox2/css/lightbox.css'
	];

	grunt.initConfig({
		// running `grunt less` will compile once
		less: {
			development: {
				options: {
					paths: ["./css"],
					yuicompress: true
				},
				files: {
					"./css/main.css": "./less/main.less",
					"./css/ms/main.css": "./less/ms/main.less",
				}
			},
		},

		// Concat all assets to one single file (for css and js)
		concat: {
			/*
		    options: {
		      separator: '',
		      stripBanners: true
		    },			
		    */
	        css : {
		      src: cssFiles,
		      dest: 'css/build.css'
	        },			
	        js : {
		      src: jsFiles,
		      dest: 'js/build.js'
	        }					
		},		
		// running `grunt watch` will watch for changes
		watch: {
			files: ['less/**/*.less'], // which files to watch
			tasks: ["less", "concat:js"],
			options: {
				atBegin: true
			}
		}
	
});
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');	

	grunt.registerTask('default', ['watch']);
};